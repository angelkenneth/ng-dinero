# Angular Dinero

Integrate [Dinero.js](https://www.npmjs.com/package/dinero.js)
    into [Angular](https://www.npmjs.com/package/@angular/core)
    and [Angular Material](https://www.npmjs.com/package/@angular/material).

## Development

1. Docker and Docker compose is required.
1. Initialize with `npm run install`.
1. Run `touch project.env`.

### Relevant Commands

1. `npm run local:install`
1. `npm run start`
1. `npm run start:storybook`
1. `npm run build`
1. `npm run build:storybook`
1. `npm run serve:storybook`
1. `npm run build,serve:storybook`
1. `npm run test`
1. `npm run lint`

### Generating Script

```bash
docker run -it -v $PWD:/init -w /init node:10.15.3 su node

NG_CLI_ANALYTICS=true npx create-nx-workspace ng-dinero --preset=empty --cli=angular --style=scss \
    && cd ng-dinero \
    && npx ng add @nrwl/angular --defaults \
    && npx ng g @nrwl/angular:library core --publishable --style=scss --prefix=ngx \
    && npx ng g @nrwl/angular:library field --publishable --style=scss --prefix=ngx \
    && npx ng g @nrwl/angular:library mat-field --publishable --style=scss --prefix=ngx \
    && npx ng g @nrwl/angular:application documentation --directory --routing --prefix=dn --style=scss --skip-tests \
    && npx -p @storybook/cli sb init --type angular \
    && npm install \
        "dinero.js@<1.6" \
        @angular/cdk \
        @angular/material \
        cleave.js \
        currency-codes \
        currency.js \
        ngx-cleave-directive \
    && npm install -D \
        "@types/dinero.js@<1.6" \
        @storybook/addon-centered \
        @storybook/addon-knobs \
        @storybook/theming \
        angular-http-server \
        awesome-typescript-loader \
        concurrently \
        css-loader \
        http-server \
        style-loader \
    && echo "Done"
```
