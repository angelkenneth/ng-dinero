module.exports = {
  stories: [
    '../libs/**/*.stories.ts',
  ],
  addons: [
    '@storybook/addon-knobs',
    '@storybook/addon-actions',
    '@storybook/addon-links',
    '@storybook/addon-notes',
  ],
  webpackFinal: async (config, { configType }) => {

    config.module.rules.push({
      test: /\.(ts|tsx)$/,
      use: [
        {
          loader: require.resolve('awesome-typescript-loader'),
        },
      ],
    });

    config.module.rules.push({
      test: /\.css$/,
      sideEffects: true,
      use: ['style-loader', 'css-loader'],
    });

    return config;
  },
};
