module.exports = {
  name: 'mat-field',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/mat-field',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js',
  ],
};
