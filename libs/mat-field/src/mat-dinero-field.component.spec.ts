import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldAppearance, MatFormFieldModule } from '@angular/material/form-field';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatDineroFieldModule } from './mat-dinero-field.module';

class TestState {
  constructor(
    public readonly fixture: ComponentFixture<MatDineroFieldTestComponent>,
  ) {
  }

  get component() {
    return this.fixture.componentInstance;
  }

  get select$() {
    return this.fixture.nativeElement.querySelector('select');
  }

  get input$() {
    return this.fixture.nativeElement.querySelector('input')!;
  }

  exists(selectors: string) {
    return !!this.fixture.nativeElement.querySelector(selectors);
  }
}


@Component({
  template: `
    <mat-form-field [appearance]="appearance">
      <mat-label>Price</mat-label>
      <ngx-dinero-field [formControl]="control" [currencies]="currencies"></ngx-dinero-field>
      <mat-hint>Some money</mat-hint>
      <mat-error>Some Error</mat-error>
    </mat-form-field>`,
})
class MatDineroFieldTestComponent {
  control = new FormControl('');
  currencies = ['AUD'];
  appearance: MatFormFieldAppearance = 'standard';
}


describe('MatDineroFieldComponent', () => {
  let state: TestState;

  beforeEach(() => {
    TestBed
      .configureTestingModule({
        declarations: [MatDineroFieldTestComponent],
        imports: [
          MatDineroFieldModule,
          MatFormFieldModule,
          NoopAnimationsModule,
          ReactiveFormsModule,
        ],
      })
      .compileComponents();
    state = new TestState(TestBed.createComponent(MatDineroFieldTestComponent));
    state.fixture.detectChanges();
  });

  describe('<mat-form-field/>', () => {
    describe('<mat-hint/>', () => {
      describe('input amount element', () => {
        describe('given its value is empty', () => {
          beforeEach(() => {
            state.input$.value = '';
            state.input$.dispatchEvent(new Event('input'));
            state.fixture.detectChanges();
          });
          it('should not float', () => {
            expect(state.exists('.mat-form-field-should-float')).toBeFalsy();
          });
        });
        describe('given its value is non-empty', () => {
          beforeEach(() => {
            state.input$.value = '20.00';
            state.input$.dispatchEvent(new Event('input'));
            state.fixture.detectChanges();
          });
          it('should float', () => {
            expect(state.exists('.mat-form-field-should-float')).toBeTruthy();
          });
        });
      });
    });
  });
});

// TODO required, disabled, etc.
// TODO if single field, invalid currency, choosing valid currency switches focus to input
/* TODO test relating to FormField
 * Stories:
 * - Value: null
 * - Valid object
 * - Invalid object
 * - Custom placeholder
 * - Focused
 * - Blur
 * - Empty
 * - Various MatFormFieldAppearance = 'legacy' | 'standard' | 'fill' | 'outline'
 * - Required
 * - Disabled
 * - Amount Error
 * - Currency Error
 */
