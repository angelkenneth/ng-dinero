import '!style-loader!css-loader!@angular/material/prebuilt-themes/indigo-pink.css';
import { FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormFieldAppearance, MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DineroFieldErrorCode } from '@ng-dinero/core';
import { action } from '@storybook/addon-actions';
import { centered } from '@storybook/addon-centered/angular';
import { text, withKnobs } from '@storybook/addon-knobs';
import { StoryContext } from '@storybook/addons/dist/types';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { MatDineroFieldModule } from './mat-dinero-field.module';


const currencyTuples = [
  { name: 'Single Currency', currencies: ['AUD'] },
  { name: 'Multiple Currency', currencies: ['AUD', 'USD'] },
];


function dineroFieldStory(kind: string) {
  return storiesOf(kind, module)
    .addDecorator(centered)
    .addDecorator(withKnobs)
    .addDecorator(
      moduleMetadata({
        imports: [
          BrowserAnimationsModule,
          MatDineroFieldModule,
          MatFormFieldModule,
          ReactiveFormsModule,
        ],
      }),
    );
}


for (const { name, currencies } of currencyTuples) {
  dineroFieldStory(`DineroFieldComponent / Form Field / ${name}`)
    .add('Blank', () => {
      const onChange = action('Input Changed');
      const control = new FormControl('');
      control.valueChanges.subscribe(onChange);
      return {
        ...floatStateColumns(`
            <mat-label>Item Price</mat-label>
            <ngx-dinero-field [formControl]="control" [currencies]="currencies"></ngx-dinero-field>
        `),
        props: { control, currencies },
      };
    })
    .add('Valued', () => {
      const onChange = action('Input Changed');
      const control = new FormControl({ amount: 100, currency: 'AUD' });
      control.valueChanges.subscribe(onChange);
      return {
        ...floatStateColumns(`
            <mat-label>Item Price</mat-label>
            <ngx-dinero-field [formControl]="control" [currencies]="currencies"></ngx-dinero-field>
        `),
        props: { control, currencies },
      };
    })
    .add('w/ Placeholder', () => {
      const onChange = action('Input Changed');
      const control = new FormControl('');
      control.valueChanges.subscribe(onChange);
      return {
        ...floatStateColumns(`
            <mat-label>Item Price</mat-label>
            <ngx-dinero-field [formControl]="control" [currencies]="currencies" [placeholder]="placeholder"></ngx-dinero-field>
        `),
        props: { control, currencies, placeholder: text('Placeholder', 'A$ 0.00') },
      };
    })
    .add('Disabled', () => {
      const onChange = action('Input Changed');
      const controlEmpty = new FormControl('');
      controlEmpty.disable();
      controlEmpty.valueChanges.subscribe(onChange);
      const controlValued = new FormControl({ amount: 100, currency: 'AUD' });
      controlValued.disable();
      controlValued.valueChanges.subscribe(onChange);
      return {
        ...floatStateValuedColumns(`
            <mat-label>Item Price</mat-label>
            <ngx-dinero-field [formControl]="controlEmpty" [currencies]="currencies"></ngx-dinero-field>
        `, `
            <mat-label>Item Price</mat-label>
            <ngx-dinero-field [formControl]="controlValued" [currencies]="currencies"></ngx-dinero-field>
        `),
        props: { controlEmpty, controlValued, currencies },
      };
    })
    .add(`Required Attribute`, () => {
      const onChange = action('Input Changed');
      const untouchedControl = new FormControl('');
      untouchedControl.valueChanges.subscribe(onChange);
      const touchedControl = new FormControl('');
      touchedControl.markAsTouched();
      touchedControl.valueChanges.subscribe(onChange);
      const validControl = new FormControl({ amount: 100, currency: 'AUD' });
      validControl.markAsTouched();
      validControl.valueChanges.subscribe(onChange);
      return {
        ...touchStateColumns(`
            <mat-label>Item Price</mat-label>
            <ngx-dinero-field [formControl]="untouchedControl" [currencies]="currencies" required></ngx-dinero-field>
            <mat-error *ngIf=" untouchedControl.hasError(errorCode) ">Required</mat-error>
        `, `
            <mat-label>Item Price</mat-label>
            <ngx-dinero-field [formControl]="touchedControl" [currencies]="currencies" required></ngx-dinero-field>
            <mat-error *ngIf=" touchedControl.hasError(errorCode) ">Required</mat-error>
        `, `
            <mat-label>Item Price</mat-label>
            <ngx-dinero-field [formControl]="validControl" [currencies]="currencies" required></ngx-dinero-field>
            <mat-error *ngIf=" validControl.hasError(errorCode) ">Required</mat-error>
        `),
        props: { untouchedControl, touchedControl, validControl, currencies, errorCode: DineroFieldErrorCode.required },
      };
    })
    .add(`Required Validator`, () => {
      const onChange = action('Input Changed');
      const untouchedControl = new FormControl('', Validators.required);
      untouchedControl.valueChanges.subscribe(onChange);
      const touchedControl = new FormControl('', Validators.required);
      touchedControl.markAsTouched();
      touchedControl.valueChanges.subscribe(onChange);
      const validControl = new FormControl({ amount: 100, currency: 'AUD' }, Validators.required);
      validControl.markAsTouched();
      validControl.valueChanges.subscribe(onChange);
      return {
        ...touchStateColumns(`
            <mat-label>Item Price</mat-label>
            <ngx-dinero-field [formControl]="untouchedControl" [currencies]="currencies"></ngx-dinero-field>
            <mat-error *ngIf=" untouchedControl.hasError(errorCode) ">Required</mat-error>
        `, `
            <mat-label>Item Price</mat-label>
            <ngx-dinero-field [formControl]="touchedControl" [currencies]="currencies"></ngx-dinero-field>
            <mat-error *ngIf=" touchedControl.hasError(errorCode) ">Required</mat-error>
        `, `
            <mat-label>Item Price</mat-label>
            <ngx-dinero-field [formControl]="validControl" [currencies]="currencies"></ngx-dinero-field>
            <mat-error *ngIf=" validControl.hasError(errorCode) ">Required</mat-error>
        `),
        props: { untouchedControl, touchedControl, validControl, currencies, errorCode: DineroFieldErrorCode.required },
      };
    })
    .add(`Error: ${DineroFieldErrorCode.invalidChoice}`, () => {
      const onChange = action('Input Changed');
      const untouchedControl = new FormControl({ amount: 100, currency: 'EUR' });
      untouchedControl.valueChanges.subscribe(onChange);
      const touchedControl = new FormControl({ amount: 100, currency: 'EUR' });
      touchedControl.markAsTouched();
      touchedControl.valueChanges.subscribe(onChange);
      const validControl = new FormControl({ amount: 100, currency: 'AUD' });
      validControl.markAsTouched();
      validControl.valueChanges.subscribe(onChange);
      return {
        ...touchStateColumns(`
            <mat-label>Item Price</mat-label>
            <ngx-dinero-field [formControl]="untouchedControl" [currencies]="currencies"></ngx-dinero-field>
            <mat-error *ngIf=" untouchedControl.hasError(errorCode) ">{{ untouchedControl.getError(errorCode).message }}</mat-error>
        `, `
            <mat-label>Item Price</mat-label>
            <ngx-dinero-field [formControl]="touchedControl" [currencies]="currencies"></ngx-dinero-field>
            <mat-error *ngIf=" touchedControl.hasError(errorCode) ">{{ touchedControl.getError(errorCode).message }}</mat-error>
        `, `
            <mat-label>Item Price</mat-label>
            <ngx-dinero-field [formControl]="validControl" [currencies]="currencies"></ngx-dinero-field>
            <mat-error *ngIf=" validControl.hasError(errorCode) ">{{ validControl.getError(errorCode).message }}</mat-error>
        `),
        props: {
          untouchedControl,
          touchedControl,
          validControl,
          currencies,
          errorCode: DineroFieldErrorCode.invalidChoice,
        },
      };
    })
  ;
}


function floatStateColumns(template: string): Pick<StoryContext, 'styles' | 'template'> {
  const appearances: MatFormFieldAppearance[] = ['legacy', 'standard', 'fill', 'outline'];
  const templates = appearances.map(appearance => `
    <mat-form-field [appearance]="'${appearance}'">${template}</mat-form-field>
    <mat-form-field [appearance]="'${appearance}'" floatLabel="always">${template}</mat-form-field>
  `);

  // language=CSS
  const style = `form {
    background: white;
    display: grid;
    grid-auto-flow: row;
    grid-gap: 16px;
    grid-template-columns: repeat(2, 1fr);
    padding: 16px;
  }`;
  return {
    styles: [style],
    template: `<form>${templates.join('')}</form>`,
  };
}


function floatStateValuedColumns(
  template1: string,
  template2: string,
): Pick<StoryContext, 'styles' | 'template'> {
  const appearances: MatFormFieldAppearance[] = ['legacy', 'standard', 'fill', 'outline'];
  const templates = appearances.map(appearance => `
    <mat-form-field [appearance]="'${appearance}'">${template1}</mat-form-field>
    <mat-form-field [appearance]="'${appearance}'" floatLabel="always">${template1}</mat-form-field>
    <mat-form-field [appearance]="'${appearance}'">${template2}</mat-form-field>
  `);

  // language=CSS
  const style = `form {
    background: white;
    display: grid;
    grid-auto-flow: row;
    grid-gap: 16px;
    grid-template-columns: repeat(3, 1fr);
    padding: 16px;
  }`;
  return {
    styles: [style],
    template: `
    <form>
      <h3>Empty (default)</h3>
      <h3>Empty (floatLabel="always")</h3>
      <h3>Valued</h3>
      ${templates.join('')}
    </form>`,
  };
}


function touchStateColumns(
  untouchedTemplate: string,
  touchedTemplate: string,
  validTemplate: string,
): Pick<StoryContext, 'styles' | 'template'> {
  const appearances: MatFormFieldAppearance[] = ['legacy', 'standard', 'fill', 'outline'];
  const templates = appearances.map(appearance => `
    <mat-form-field [appearance]="'${appearance}'">${untouchedTemplate}</mat-form-field>
    <mat-form-field [appearance]="'${appearance}'" floatLabel="always">${untouchedTemplate}</mat-form-field>
    <mat-form-field [appearance]="'${appearance}'">${touchedTemplate}</mat-form-field>
    <mat-form-field [appearance]="'${appearance}'" floatLabel="always">${touchedTemplate}</mat-form-field>
    <mat-form-field [appearance]="'${appearance}'">${validTemplate}</mat-form-field>
  `);

  // language=CSS
  const style = `form {
    background: white;
    display: grid;
    grid-auto-flow: row;
    grid-gap: 16px;
    grid-template-columns: repeat(5, 1fr);
    padding: 16px;
  }`;
  return {
    styles: [style],
    template: `<form>
        <h3>Untouched</h3>
        <h3></h3>
        <h3>Touched</h3>
        <h3></h3>
        <h3>Valid</h3>
        ${templates.join('')}
    </form>`,
  };
}
