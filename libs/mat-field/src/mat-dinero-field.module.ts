import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxCleaveDirectiveModule } from 'ngx-cleave-directive';
import { MatDineroFieldComponent } from './mat-dinero-field.component';


@NgModule({
  declarations: [MatDineroFieldComponent],
  exports: [MatDineroFieldComponent],
  imports: [
    CommonModule,
    NgxCleaveDirectiveModule,
    ReactiveFormsModule,
  ],
})
export class MatDineroFieldModule {
}
