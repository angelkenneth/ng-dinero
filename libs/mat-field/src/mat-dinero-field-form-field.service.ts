import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { Inject, Injectable, OnDestroy, Optional, Self } from '@angular/core';
import { FormControl, NgControl } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material/form-field';
import {
  AMOUNT_CONTROL,
  AMOUNT_ELEMENT,
  CURRENCY_CONTROL,
  CURRENCY_ELEMENT,
  CURRENCY_TUPLES,
  CurrencyTuple,
  DineroFieldService,
  DineroFieldValueAccessorService,
  DineroPartialObject,
} from '@ng-dinero/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { MatDineroFieldService } from './mat-dinero-field.service';


@Injectable({ providedIn: 'root' })
export class MatDineroFieldFormFieldService implements MatFormFieldControl<DineroPartialObject | null>, OnDestroy {
  constructor(
    @Inject(AMOUNT_CONTROL) public readonly amountControl: FormControl,
    @Inject(CURRENCY_CONTROL) public readonly currencyControl: FormControl,
    @Inject(CURRENCY_TUPLES) public readonly currencyTuples: BehaviorSubject<CurrencyTuple[]>,
    @Inject(AMOUNT_ELEMENT) public readonly amountInput$: BehaviorSubject<HTMLInputElement | null>,
    @Inject(CURRENCY_ELEMENT) public readonly currencySelect$: BehaviorSubject<HTMLSelectElement | null>,
    @Optional() @Self() public ngControl: NgControl,
    private readonly dineroFieldService: DineroFieldService,
    private readonly matDineroFieldService: MatDineroFieldService,
    private readonly accessorService: DineroFieldValueAccessorService,
  ) {
  }

  private _value: DineroPartialObject | null = null;
  public stateChanges = new Subject<void>();
  public static nextId = 0;
  public id = `dinero-field-${MatDineroFieldFormFieldService.nextId++}`;
  private _placeholder = '';
  public focused = false;
  public empty = false;
  public shouldLabelFloat = false;
  private _required = false;
  private _disabled = false;
  public controlType = 'dinero-field';
  public describedBy = '';

  private readonly changeSub = this.dineroFieldService.changeRawValue.pipe(
    tap(object => this._value = object),
  ).subscribe();
  private readonly isFocusedSub = this.matDineroFieldService.isFocused.pipe(
    tap(isFocused => this.focused = isFocused),
  ).subscribe();
  private readonly isEmptySub = this.matDineroFieldService.isEmpty.pipe(
    tap(isEmpty => this.empty = isEmpty),
  ).subscribe();
  private readonly shouldLabelFloatSub = this.matDineroFieldService.shouldLabelFloat.pipe(
    tap(itShould => this.shouldLabelFloat = itShould),
  ).subscribe();

  ngOnDestroy() {
    this.changeSub.unsubscribe();
    this.isFocusedSub.unsubscribe();
    this.isEmptySub.unsubscribe();
    this.shouldLabelFloatSub.unsubscribe();
  }

  set value(object: DineroPartialObject | null) {
    this._value = object;
    this.accessorService.writeValue(object);
  }

  get value() {
    return this._value;
  }

  get placeholder() {
    return this._placeholder;
  }

  set placeholder(plh) {
    this._placeholder = plh;
    this.stateChanges.next();
  }

  get required() {
    return this._required;
  }

  set required(req: boolean) {
    this._required = coerceBooleanProperty(req);
    this.stateChanges.next();
  }

  get disabled(): boolean {
    return this._disabled;
  }

  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);
    this.accessorService.setDisabledState(this._disabled);
    this.stateChanges.next();
  }

  get errorState(): boolean {
    // Credit: https://stackoverflow.com/a/54206099
    return this.ngControl.errors !== null && !!this.ngControl.touched;
  }

  setDescribedByIds(ids: string[]) {
    this.describedBy = ids.join(' ');
  }

  onContainerClick(event: MouseEvent) {
    const currencySelect$ = this.currencySelect$.value;
    const amountInput$ = this.amountInput$.value!;
    const clicked$ = event.target as Element;

    if (!(
      currencySelect$ && clicked$.isSameNode(currencySelect$)
      || clicked$.isSameNode(amountInput$)
    )) {
      amountInput$.focus();
    }
  }
}
