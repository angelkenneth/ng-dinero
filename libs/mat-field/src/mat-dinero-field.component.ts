import { Component, HostBinding, Inject, OnDestroy } from '@angular/core';
import { FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material/form-field';
import {
  AMOUNT_CONTROL,
  AMOUNT_ELEMENT,
  CURRENCY_CONTROL,
  CURRENCY_ELEMENT,
  CURRENCY_TUPLES,
  CurrencyTuple,
  DineroFieldService,
  DineroFieldValidatorService,
  DineroFieldValueAccessorService,
  sortedCurrencies,
} from '@ng-dinero/core';
import { DineroFieldComponent } from '@ng-dinero/field';
import { BehaviorSubject } from 'rxjs';
import { MatDineroFieldFormFieldService } from './mat-dinero-field-form-field.service';
import { MatDineroFieldService } from './mat-dinero-field.service';


@Component({
  selector: 'ngx-dinero-field',
  templateUrl: '../../field/src/dinero-field.component.html',
  styleUrls: ['./mat-dinero-field.component.scss'],
  providers: [
    { provide: AMOUNT_CONTROL, useFactory: () => new FormControl('') },
    { provide: CURRENCY_CONTROL, useFactory: () => new FormControl('') },
    // TODO have this be dependent on Token
    { provide: CURRENCY_TUPLES, useFactory: () => new BehaviorSubject<CurrencyTuple[]>(sortedCurrencies()) },
    { provide: AMOUNT_ELEMENT, useFactory: () => new BehaviorSubject<HTMLInputElement | null>(null) },
    { provide: CURRENCY_ELEMENT, useFactory: () => new BehaviorSubject<HTMLSelectElement | null>(null) },
    MatDineroFieldService,
    { provide: DineroFieldService, useExisting: MatDineroFieldService },
    DineroFieldValueAccessorService,
    DineroFieldValidatorService,
    MatDineroFieldFormFieldService,
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: DineroFieldValueAccessorService,
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: DineroFieldValidatorService,
      multi: true,
    },
    {
      provide: MatFormFieldControl,
      useExisting: MatDineroFieldFormFieldService,
    },
  ],
})
export class MatDineroFieldComponent extends DineroFieldComponent implements OnDestroy {
  constructor(
    @Inject(AMOUNT_CONTROL) amountControl: FormControl,
    @Inject(CURRENCY_CONTROL) currencyControl: FormControl,
    @Inject(CURRENCY_TUPLES) currencyTuples: BehaviorSubject<CurrencyTuple[]>,
    @Inject(AMOUNT_ELEMENT) amountInput$: BehaviorSubject<HTMLInputElement | null>,
    @Inject(CURRENCY_ELEMENT) currencySelect$: BehaviorSubject<HTMLSelectElement | null>,
    dineroFieldService: DineroFieldService,
    accessorService: DineroFieldValueAccessorService,
    validatorService: DineroFieldValidatorService,
    private readonly matDineroFieldService: MatDineroFieldService,
    private readonly formFieldService: MatDineroFieldFormFieldService,
  ) {
    super(
      amountControl,
      currencyControl,
      currencyTuples,
      amountInput$,
      currencySelect$,
      dineroFieldService,
      accessorService,
      validatorService,
    );
  }

  private _showCurrencySelect = true;
  private readonly showCurrencySelectSub = this.dineroFieldService.showCurrencySelect.subscribe(tf => this._showCurrencySelect = tf);

  @HostBinding('attr.aria-describedby') get describedBy() {
    return this.formFieldService.describedBy;
  }

  @HostBinding('class.showing-currency-field') get showingCurrencyField() {
    return this._showCurrencySelect;
  }

  @HostBinding('class.inside-mat-form-field') get isInsideMatFormField() {
    return this.matDineroFieldService.isInsideMatFormField;
  }

  @HostBinding('class.vanilla') get isVanilla() {
    return this.matDineroFieldService.isVanilla;
  }

  @HostBinding('class.label-not-floating') get isLabelNotFloating() {
    return this.matDineroFieldService.isLabelNotFloating;
  }

  ngOnDestroy(): void {
    this.showCurrencySelectSub.unsubscribe();
  }
}
