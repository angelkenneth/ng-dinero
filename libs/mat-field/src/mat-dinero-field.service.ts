import { FocusMonitor } from '@angular/cdk/a11y';
import { Inject, Injectable, Optional } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatFormField } from '@angular/material/form-field';
import {
  AMOUNT_CONTROL,
  AMOUNT_ELEMENT,
  areAnyTruthy,
  CURRENCY_CONTROL,
  CURRENCY_ELEMENT,
  CURRENCY_TUPLES,
  CurrencyTuple,
  DineroFieldService,
} from '@ng-dinero/core';
import { BehaviorSubject, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root',
})
export class MatDineroFieldService extends DineroFieldService {
  constructor(
    @Inject(AMOUNT_CONTROL) amountControl: FormControl,
    @Inject(CURRENCY_CONTROL) currencyControl: FormControl,
    @Inject(CURRENCY_TUPLES) currencyTuples: BehaviorSubject<CurrencyTuple[]>,
    @Inject(AMOUNT_ELEMENT) amountInput$: BehaviorSubject<HTMLInputElement | null>,
    @Inject(CURRENCY_ELEMENT) currencySelect$: BehaviorSubject<HTMLSelectElement | null>,
    private readonly fm: FocusMonitor,
    @Optional() private readonly matFormField: MatFormField,
  ) {
    super(
      amountControl,
      currencyControl,
      currencyTuples,
      amountInput$,
      currencySelect$,
    );
  }

  public readonly isFocusedAmount = this.amountInput$.pipe(switchMap(el$ => el$ ? this.fm.monitor(el$) : of(false)));
  public readonly isFocusedCurrency = this.currencySelect$.pipe(switchMap(el$ => el$ ? this.fm.monitor(el$) : of(false)));
  public readonly isFocused = areAnyTruthy(this.isFocusedAmount, this.isFocusedCurrency);
  public readonly shouldLabelFloat = areAnyTruthy(this.isNotEmpty, this.isFocused);

  get isInsideMatFormField(): boolean {
    return !!this.matFormField;
  }

  get isVanilla(): boolean {
    return !this.matFormField;
  }

  get isLabelNotFloating(): boolean {
    return this.matFormField ? !this.matFormField._shouldLabelFloat() : false;
  }
}
