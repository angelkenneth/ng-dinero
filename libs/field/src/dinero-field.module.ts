import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxCleaveDirectiveModule } from 'ngx-cleave-directive';
import { DineroFieldComponent } from './dinero-field.component';


@NgModule({
  declarations: [DineroFieldComponent],
  exports: [DineroFieldComponent],
  imports: [
    CommonModule,
    NgxCleaveDirectiveModule,
    ReactiveFormsModule,
  ],
})
export class DineroFieldModule {
}
