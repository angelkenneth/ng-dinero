import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { DineroFieldErrorCode } from '@ng-dinero/core';
import { action } from '@storybook/addon-actions';
import { centered } from '@storybook/addon-centered/angular';
import { text, withKnobs } from '@storybook/addon-knobs';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { DineroFieldModule } from './dinero-field.module';


const currencyTuples = [
  { name: 'Single Currency', currencies: ['AUD'] },
  { name: 'Multiple Currency', currencies: ['AUD', 'USD'] },
];


function dineroFieldStory(kind: string) {
  return storiesOf(kind, module)
    .addDecorator(centered)
    .addDecorator(withKnobs)
    .addDecorator(
      moduleMetadata({
        imports: [
          DineroFieldModule,
          ReactiveFormsModule,
        ],
      }),
    );
}


for (const { name, currencies } of currencyTuples) {
  dineroFieldStory(`DineroFieldComponent / Vanilla / ${name}`)
    .add('Blank', () => {
      const onChange = action('Input Changed');
      const control = new FormControl('');
      control.valueChanges.subscribe(onChange);
      return {
        styles: ['main { background: white; padding: 16px }'],
        template: `<main><ngx-dinero-field [formControl]="control" [currencies]="currencies"></ngx-dinero-field></main>`,
        props: { control, currencies },
      };
    })
    .add('Valued', () => {
      const onChange = action('Input Changed');
      const control = new FormControl({ amount: 100, currency: 'AUD' });
      control.valueChanges.subscribe(onChange);
      return {
        styles: ['main { background: white; padding: 16px }'],
        template: `<main><ngx-dinero-field [formControl]="control" [currencies]="currencies"></ngx-dinero-field></main>`,
        props: { control, currencies },
      };
    })
    .add('w/ Placeholder', () => {
      const onChange = action('Input Changed');
      const control = new FormControl('');
      control.valueChanges.subscribe(onChange);
      return {
        styles: ['main { background: white; padding: 16px }'],
        template: `<main><ngx-dinero-field [formControl]="control" [placeholder]="placeholder" [currencies]="currencies"></ngx-dinero-field></main>`,
        props: { control, currencies, placeholder: text('Placeholder', 'Price of Item') },
      };
    })
    .add('Disabled', () => {
      const onChange = action('Input Changed');
      const control = new FormControl({ amount: 100, currency: 'AUD' });
      control.disable();
      control.valueChanges.subscribe(onChange);
      return {
        styles: ['main { background: white; padding: 16px }'],
        template: `<main><ngx-dinero-field [formControl]="control" [currencies]="currencies"></ngx-dinero-field></main>`,
        props: { control, currencies },
      };
    })
    .add(`Error: ${DineroFieldErrorCode.invalidChoice}`, () => {
      const onChange = action('Input Changed');
      const control = new FormControl({ amount: 100, currency: 'EUR' });
      control.valueChanges.subscribe(onChange);
      return {
        styles: ['main { background: white; padding: 16px }'],
        template: `<main>
          <ngx-dinero-field [formControl]="control" [currencies]="currencies"></ngx-dinero-field>
          <br/>
          <div>Error: {{ control.getError(errorCode)?.message || '! Not expecting no errors !' }}</div>
        </main>`,
        props: { control, currencies, errorCode: DineroFieldErrorCode.invalidChoice },
      };
    })
  ;
}
