import { Component, ElementRef, Inject, Input, OnDestroy, ViewChild } from '@angular/core';
import { FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import {
  AMOUNT_CONTROL,
  AMOUNT_ELEMENT,
  CURRENCY_CONTROL,
  CURRENCY_ELEMENT,
  CURRENCY_TUPLES,
  CurrencyTuple,
  DineroFieldService,
  DineroFieldValidatorService,
  DineroFieldValueAccessorService,
  getAsCurrencyTuples,
  sortedCurrencies,
} from '@ng-dinero/core';
import { BehaviorSubject } from 'rxjs';


export function newAmountControl() {
  return new FormControl('');
}

export function newCurrencyControl() {
  return new FormControl('');
}

export function newCurrencyTuples() {
  return new BehaviorSubject<CurrencyTuple[]>(sortedCurrencies());
}

export function newAmountElement() {
  return new BehaviorSubject<HTMLInputElement | null>(null);
}

export function newCurrencyElement() {
  return new BehaviorSubject<HTMLSelectElement | null>(null);
}


@Component({
  selector: 'ngx-dinero-field',
  templateUrl: './dinero-field.component.html',
  styleUrls: ['./dinero-field.component.scss'],
  providers: [
    { provide: AMOUNT_CONTROL, useFactory: newAmountControl },
    { provide: CURRENCY_CONTROL, useFactory: newCurrencyControl },
    // TODO have this be dependent on Token
    { provide: CURRENCY_TUPLES, useFactory: newCurrencyTuples },
    { provide: AMOUNT_ELEMENT, useFactory: newAmountElement },
    { provide: CURRENCY_ELEMENT, useFactory: newCurrencyElement },
    DineroFieldService,
    DineroFieldValueAccessorService,
    DineroFieldValidatorService,
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: DineroFieldValueAccessorService,
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: DineroFieldValidatorService,
      multi: true,
    },
  ],
})
export class DineroFieldComponent implements OnDestroy {
  constructor(
    @Inject(AMOUNT_CONTROL) public readonly amountControl: FormControl,
    @Inject(CURRENCY_CONTROL) public readonly currencyControl: FormControl,
    @Inject(CURRENCY_TUPLES) public readonly currencyTuples: BehaviorSubject<CurrencyTuple[]>,
    @Inject(AMOUNT_ELEMENT) public readonly amountInput$: BehaviorSubject<HTMLInputElement | null>,
    @Inject(CURRENCY_ELEMENT) public readonly currencySelect$: BehaviorSubject<HTMLSelectElement | null>,
    protected readonly dineroFieldService: DineroFieldService,
    protected readonly accessorService: DineroFieldValueAccessorService,
    protected readonly validatorService: DineroFieldValidatorService,
  ) {
  }

  public readonly showCurrencySelect = this.dineroFieldService.showCurrencySelect;
  public readonly invalidCurrency = this.dineroFieldService.invalidCurrency;
  public readonly cleaveOptions = this.dineroFieldService.cleaveOptions;

  @Input() placeholder = '';

  @Input() set currencies(currencies: string[]) {
    this.currencyTuples.next(getAsCurrencyTuples(...currencies));
  }

  @ViewChild('currencySelectEl', { static: false }) set currencySelectEl(el$: ElementRef<HTMLSelectElement>) {
    this.currencySelect$.next(el$ ? el$.nativeElement : null);
  }

  @ViewChild('amountInputEl', { static: false }) set amountInputEl(el$: ElementRef<HTMLInputElement>) {
    this.amountInput$.next(el$ ? el$.nativeElement : null);
  }

  ngOnDestroy(): void {
    this.dineroFieldService.ngOnDestroy();
    this.accessorService.ngOnDestroy();
    this.validatorService.ngOnDestroy();
  }

  wasTouched() {
    this.accessorService.propagateTouch();
  }
}
