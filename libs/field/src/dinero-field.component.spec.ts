import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { DineroFieldErrorCode, getAsCurrencyTuple, ValidationErrorItem } from '@ng-dinero/core';
import { DineroFieldModule } from './dinero-field.module';


interface DineroFieldTestComponent {
  currencies: string[];
  control: FormControl;
}

class DineroFieldFixture {
  constructor(public readonly fixture: ComponentFixture<DineroFieldTestComponent>) {
  }

  get component() {
    return this.fixture.componentInstance;
  }

  get select$() {
    return this.fixture.nativeElement.querySelector('select');
  }

  get input$() {
    return this.fixture.nativeElement.querySelector('input');
  }
}


async function createTestBed(template: string): Promise<DineroFieldFixture> {
  @Component({ template })
  class DineroFieldHotTestComponent implements DineroFieldTestComponent {
    currencies = ['AUD'];
    control = new FormControl();
  }

  await TestBed
    .configureTestingModule({
      imports: [
        ReactiveFormsModule,
        DineroFieldModule,
      ],
      declarations: [DineroFieldHotTestComponent],
    })
    .compileComponents();
  const fixture = TestBed.createComponent(DineroFieldHotTestComponent);
  const component = fixture.componentInstance;
  component.currencies = ['AUD'];
  fixture.detectChanges();
  return new DineroFieldFixture(fixture);
}


describe('DineroFieldComponent', () => {
  let fixture: DineroFieldFixture;

  beforeEach(() => createTestBed('<ngx-dinero-field [formControl]="control" [currencies]="currencies"></ngx-dinero-field>')
    .then(f => fixture = f));

  describe('amount input element', () => {
    describe('`.value` state', () => {
      describe('given one choice of currency', () => {
        describe('given [formControl]\'s value', () => {
          describe('is null', () => {
            beforeEach(() => {
              fixture.component.control.setValue(null);
              fixture.fixture.detectChanges();
            });
            it('should be an empty string', () => {
              expect(fixture.input$!.value).toEqual('');
            });
          });
          describe('is a Dinero object', () => {
            describe('whereby its amount', () => {
              describe('has no cents', () => {
                beforeEach(() => {
                  fixture.component.control.setValue({ amount: 1000, currency: 'AUD' });
                  fixture.fixture.detectChanges();
                });
                it('should be formatted', () => {
                  expect(fixture.input$!.value).toEqual('A$ 10');
                });
              });
              describe('has cents', () => {
                beforeEach(() => {
                  fixture.component.control.setValue({ amount: 1001, currency: 'AUD' });
                  fixture.fixture.detectChanges();
                });
                it('should be formatted', () => {
                  expect(fixture.input$!.value).toEqual('A$ 10.01');
                });
              });
            });
          });
        });
      });
    });
  });

  describe('currency select element', () => {
    describe('given one choice of currency', () => {
      it('should be hidden', () => {
        expect(fixture.select$).toBeNull();
      });
      describe('given [formControl]\'s value', () => {
        describe('is a Dinero object', () => {
          describe('given its currency is invalid', () => {
            beforeEach(() => {
              fixture.component.control.setValue({ amount: 1000, currency: 'EUR' });
              fixture.fixture.detectChanges();
            });
            it('should be visible', () => {
              expect(fixture.select$).toBeDefined();
            });
            it('should make the invalid currency as a disabled option', () => {
              const options$ = fixture.select$.getElementsByTagName('option');
              const invalidChoice = options$.item(1)!;
              expect(invalidChoice.value).toEqual('EUR');
              expect(invalidChoice.selected).toBeTruthy();
              expect(invalidChoice.disabled).toBeTruthy();
            });
            describe('changing to the valid currency', () => {
              beforeEach(() => {
                fixture.select$.selectedIndex = 2; // AUD
                fixture.select$.dispatchEvent(new Event('change'));
                fixture.fixture.detectChanges();
              });
              it('should make it hidden', () => {
                expect(fixture.select$).toBeNull();
              });
            });
          });
        });
      });
    });
    describe('given multiple currencies', () => {
      beforeEach(() => {
        fixture.component.currencies = ['AUD', 'USD']; // [0] --- , [1] , [2]
        fixture.fixture.detectChanges();
      });
      it('should be visible', () => {
        expect(fixture.select$).toBeDefined();
      });
      describe('given [formControl]\'s value', () => {
        describe('is a Dinero object', () => {
          describe('given its currency is invalid', () => {
            beforeEach(() => {
              fixture.component.control.setValue({ amount: 1000, currency: 'EUR' });
              fixture.fixture.detectChanges();
            });
            it('should make the invalid currency as a disabled option', () => {
              const options$ = fixture.select$.getElementsByTagName('option');
              const invalidChoice = options$.item(1)!;
              expect(invalidChoice.value).toEqual('EUR');
              expect(invalidChoice.selected).toBeTruthy();
              expect(invalidChoice.disabled).toBeTruthy();
            });
            it('should not make it hidden', () => {
              expect(fixture.select$).toBeDefined();
            });
          });
        });
      });
    });
  });

  describe('[formControl]', () => {
    describe('`.value` state', () => {
      describe('is null', () => {
        beforeEach(() => {
          fixture.component.control.setValue(null);
          fixture.fixture.detectChanges();
        });
        describe('given one choice of currency', () => {
          describe('amount input element', () => {
            describe('setting a decimal amount to it', () => {
              beforeEach(() => {
                fixture.input$.value = '20';
                fixture.input$.dispatchEvent(new Event('input'));
                fixture.fixture.detectChanges();
              });
              it('should change its value', () => {
                expect(fixture.component.control.value).toEqual({ amount: 2000, currency: 'AUD' });
              });
            });
          });
        });
        describe('given multiple currencies', () => {
          beforeEach(() => {
            fixture.component.currencies = ['AUD', 'USD']; // [0] --- , [1] , [2]
            fixture.fixture.detectChanges();
          });
          describe('setting just currency', () => {
            beforeEach(() => {
              fixture.select$.selectedIndex = 2;
              fixture.select$.dispatchEvent(new Event('change'));
            });
            it('should still be null', () => {
              expect(fixture.component.control.value).toEqual(null);
            });
            describe('setting amount', () => {
              describe('as blank', () => {
                beforeEach(() => {
                  fixture.input$.value = '';
                  fixture.input$.dispatchEvent(new Event('input'));
                });
                it('should be `null`', () => {
                  expect(fixture.component.control.value).toEqual(null);
                });
              });
            });
          });
          describe('setting just amount', () => {
            beforeEach(() => {
              fixture.input$.value = '20';
              fixture.input$.dispatchEvent(new Event('input'));
            });
            it('should still be null', () => {
              expect(fixture.component.control.value).toEqual(null);
            });
          });
          describe('setting currency and amount', () => {
            beforeEach(() => {
              fixture.select$.selectedIndex = 2;
              fixture.select$.dispatchEvent(new Event('change'));
              fixture.input$.value = '20';
              fixture.input$.dispatchEvent(new Event('input'));
            });
            it('should change its value', () => {
              expect(fixture.component.control.value).toEqual({ amount: 2000, currency: 'USD' });
            });
          });
        });
      });
      describe('is a Dinero object', () => {
        beforeEach(() => {
          fixture.component.control.setValue({ amount: 1000, currency: 'AUD' });
          fixture.fixture.detectChanges();
        });
        it('should read the same value', () => {
          expect(fixture.component.control.value).toEqual({ amount: 1000, currency: 'AUD' });
        });
        describe('given its currency is invalid', () => {
          beforeEach(() => {
            fixture.component.control.setValue({ amount: 1000, currency: 'EUR' });
            fixture.fixture.detectChanges();
          });
          it('should read the same value', () => {
            expect(fixture.component.control.value).toEqual({ amount: 1000, currency: 'EUR' });
          });
        });
        describe('given amount input element\'s value', () => {
          describe('is set to an amount', () => {
            beforeEach(() => {
              fixture.input$.value = '20';
              fixture.input$.dispatchEvent(new Event('input'));
              fixture.fixture.detectChanges();
            });
            it('should change its value', () => {
              expect(fixture.component.control.value).toEqual({ amount: 2000, currency: 'AUD' });
            });
          });
          describe('is set to blank', () => {
            beforeEach(() => {
              fixture.input$.value = '';
              fixture.input$.dispatchEvent(new Event('input'));
            });
            it('should be `null`', () => {
              expect(fixture.component.control.value).toEqual(null);
            });
          });
          describe('is set to just be a currency symbol', () => {
            beforeEach(() => {
              fixture.input$.value = getAsCurrencyTuple('AUD').sign;
              fixture.input$.dispatchEvent(new Event('input'));
            });
            it('should be `null`', () => {
              expect(fixture.component.control.value).toEqual(null);
            });
          });
          describe('is set to an invalid string', () => {
            beforeEach(() => {
              fixture.input$.value = getAsCurrencyTuple('AUD').sign + ' . ';
              fixture.input$.dispatchEvent(new Event('input'));
            });
            it('should be `null`', () => {
              expect(fixture.component.control.value).toEqual(null);
            });
          });
        });
      });
    });
    describe('`.touched` state', () => {
      it('should initialize as false', () => {
        expect(fixture.component.control.touched).toBeFalsy();
      });
      describe('given the amount input element', () => {
        describe('focusing on it', () => {
          beforeEach(() => {
            fixture.input$.focus();
            fixture.fixture.detectChanges();
          });
          it('should make it true', () => {
            expect(fixture.component.control.touched).toBeTruthy();
          });
        });
      });
      describe('given multiple currencies', () => {
        beforeEach(() => {
          fixture.component.currencies = ['AUD', 'USD']; // [0] --- , [1] , [2]
          fixture.fixture.detectChanges();
        });
        describe('given the currency select element', () => {
          describe('focusing on it', () => {
            beforeEach(() => {
              fixture.select$.focus();
              fixture.fixture.detectChanges();
            });
            it('should make it true', () => {
              const focused$ = fixture.fixture.debugElement.query(By.css(':focus')).nativeElement;
              expect(focused$).toBe(fixture.select$);
              expect(fixture.component.control.touched).toBeTruthy();
            });
          });
        });
      });
    });
    describe('`.errors` state', () => {
      describe('given that the currency is invalid', () => {
        beforeEach(() => {
          fixture.component.control.setValue({ amount: 1000, currency: 'EUR' });
          fixture.fixture.detectChanges();
        });
        it('should show the invalid currency as a deactivated option', () => {
          const options$ = fixture.select$.getElementsByTagName('option');
          const invalidChoice = options$.item(1)!;
          expect(invalidChoice.value).toEqual('EUR');
          expect(invalidChoice.selected).toBeTruthy();
          expect(invalidChoice.disabled).toBeTruthy();
        });
        it('should report error code: ' + DineroFieldErrorCode.invalidChoice, () => {
          const expected: Partial<ValidationErrorItem> = {
            type: DineroFieldErrorCode.invalidChoice,
            context: { valid: ['AUD'] },
          };
          expect(fixture.component.control.errors).toMatchObject({ [DineroFieldErrorCode.invalidChoice]: expected });
        });
        describe('thereafter a valid currency was set', () => {
          beforeEach(() => {
            fixture.select$.selectedIndex = 2;  // AUD
            fixture.select$.dispatchEvent(new Event('change'));
          });
          it('should clear errors', () => {
            expect(fixture.component.control.errors).toBeNull();
          });
        });
      });
    });
  });
});
