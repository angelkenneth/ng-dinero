import { InjectionToken } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { CurrencyTuple } from './utils';

export const AMOUNT_CONTROL = new InjectionToken<FormControl>('Amount Control');
export const CURRENCY_CONTROL = new InjectionToken<FormControl>('Currency Control');
export const CURRENCY_TUPLES = new InjectionToken<BehaviorSubject<CurrencyTuple[]>>('Currency Tuples');
export const AMOUNT_ELEMENT = new InjectionToken<BehaviorSubject<HTMLInputElement | null>>('Amount Element');
export const CURRENCY_ELEMENT = new InjectionToken<BehaviorSubject<HTMLSelectElement | null>>('Currency Element');
