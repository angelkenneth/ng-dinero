import { Inject, Injectable, OnDestroy } from '@angular/core';
import { ControlValueAccessor, FormControl } from '@angular/forms';
import * as Dinero_ from 'dinero.js';
import { BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { DineroFieldValidatorService } from './dinero-field-validator.service';
import { DineroPartialObject } from './dinero-field.models';
import { DineroFieldService } from './dinero-field.service';
import { AMOUNT_CONTROL, CURRENCY_CONTROL, CURRENCY_TUPLES } from './dinero-field.tokens';
import { CurrencyTuple } from './utils';


const DineroFactory = Dinero_;


@Injectable({ providedIn: 'root' })
export class DineroFieldValueAccessorService implements ControlValueAccessor, OnDestroy {
  constructor(
    @Inject(AMOUNT_CONTROL) public readonly amountControl: FormControl,
    @Inject(CURRENCY_CONTROL) public readonly currencyControl: FormControl,
    @Inject(CURRENCY_TUPLES) public readonly currencyTuples: BehaviorSubject<CurrencyTuple[]>,
    private readonly dineroFieldService: DineroFieldService,
    private readonly validatorService: DineroFieldValidatorService,
  ) {
  }

  private readonly changeSub = this.dineroFieldService.changeRawValue.pipe(
    tap(object => this.propagateNewValue(object ? object : null)),
  ).subscribe();

  public propagateChange: (value: DineroPartialObject | null) => void = () => void 0;
  public propagateTouch: () => void = () => void 0;

  ngOnDestroy() {
    this.changeSub.unsubscribe();
  }

  writeValue(obj: DineroPartialObject | null): void {
    if (obj) {
      this.dineroFieldService.writtenValue.next(DineroFactory(obj));
      this.amountControl.setValue(String(obj.amount / 100));
      this.currencyControl.setValue(obj.currency);
    } else {
      this.dineroFieldService.writtenValue.next(null);
      this.amountControl.setValue('');
      this.currencyControl.setValue('');
    }
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }

  propagateNewValue(value: DineroPartialObject | null): void {
    this.propagateChange(value);
    this.validatorService.propagateValidatorChange();
  }

  setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.amountControl.disable();
      this.currencyControl.disable();
    } else {
      this.amountControl.enable();
      this.currencyControl.enable();
    }
  }
}
