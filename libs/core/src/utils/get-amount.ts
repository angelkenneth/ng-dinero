import * as currency_ from 'currency.js';

const currency = currency_;

export function getAmount(amountStr: string): number {
  return currency(amountStr).intValue;
}
