const SIGN_DELIMITER = /\d+\.\d+/;

export interface CurrencySymbol {
  prefix: string;
  suffix: string;
  sign: string;
}

export function getCurrencySymbol(code: string): CurrencySymbol {
  const format = new Intl.NumberFormat(undefined, { style: 'currency', currency: code });
  const number = format.format(1.1);
  const [prefix, suffix] = number.split(SIGN_DELIMITER).map(s => s.trim());
  const sign = `${prefix} ${suffix}`.trim();
  return { prefix, suffix, sign };
}
