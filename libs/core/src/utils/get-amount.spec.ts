import { getAmount } from './get-amount';

describe('getAmount', () => {
  [
    { given: 'A$ 1.00', expecting: 100 },
    { given: 'A$ 1.33', expecting: 133 },
    { given: 'A$ 1.89', expecting: 189 },
  ].forEach(({ given, expecting }) => {
    describe(`given ${given}`, () => {
      it(`should return ${expecting}`, () => {
        expect(getAmount(given)).toEqual(expecting);
      });
    });
  });
});
