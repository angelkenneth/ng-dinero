import { code as fromCode, CurrencyCodeRecord } from 'currency-codes';
import { CurrencySymbol, getCurrencySymbol } from './get-currency-symbol';


export interface CurrencyTuple extends CurrencyCodeRecord, CurrencySymbol {
  id: string;
}

const SUPPORTED_CURRENCIES = ['AUD'];

export function supportedCurrencyCodes(): string[] {
  return SUPPORTED_CURRENCIES;
}

export function sortedCurrencies() {
  return getAsCurrencyTuples(...SUPPORTED_CURRENCIES);
}

export function getAsCurrencyTuples(...codes: string[]): CurrencyTuple[] {
  return codes.map(code_ => getAsCurrencyTuple(code_));
}

export function getAsCurrencyTuple(code_: string): CurrencyTuple {
  const currency = fromCode(code_);

  if (!currency) {
    throw new Error(`Currency ${code_} not found`);
  }

  return {
    id: currency.code,
    ...currency,
    ...getCurrencySymbol(currency.code),
  };
}
