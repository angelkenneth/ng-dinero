import { Dinero } from 'dinero.js';
import { Observable } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

export function distinctUntilChangedDinero() {
  return (source: Observable<Dinero | null>) => source.pipe(
    distinctUntilChanged((d1, d2) => !d1 && !d2 || d1 && d2 && d1.equalsTo(d2) || false),
  );
}
