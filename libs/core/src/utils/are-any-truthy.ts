import { combineLatest, Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

export function areAnyTruthy(...observable: Observable<any>[]): Observable<boolean> {
  return combineLatest(observable.map(o => o.pipe(startWith(null))))
    .pipe(map(([...emits]) => emits.reduce((accu, curr) => accu || curr, false)));
}
