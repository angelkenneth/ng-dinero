import { DineroObject } from 'dinero.js';
import { DineroFieldErrorCode } from './dinero-field.enums';

export type DineroPartialObject = Pick<DineroObject, 'amount' | 'currency'>;

// Copied from @hapi/joi
export interface ValidationErrorItem {
  message: string;
  path: string[];
  type: DineroFieldErrorCode;
  context?: any;
}
