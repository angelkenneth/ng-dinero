import { Inject, Injectable, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import * as Dinero_ from 'dinero.js';
import { Dinero } from 'dinero.js';
import { BehaviorSubject, combineLatest, concat, merge, Observable, of } from 'rxjs';
import { map, skip, startWith, switchMap } from 'rxjs/operators';
import {
  AMOUNT_CONTROL,
  AMOUNT_ELEMENT,
  CURRENCY_CONTROL,
  CURRENCY_ELEMENT,
  CURRENCY_TUPLES,
} from './dinero-field.tokens';
import { CurrencyTuple, distinctUntilChangedDinero, getAmount, getAsCurrencyTuple, promiseFirst } from './utils';


const DineroFactory = Dinero_;


@Injectable({
  providedIn: 'root',
})
export class DineroFieldService implements OnDestroy {
  constructor(
    @Inject(AMOUNT_CONTROL) public readonly amountControl: FormControl,
    @Inject(CURRENCY_CONTROL) public readonly currencyControl: FormControl,
    @Inject(CURRENCY_TUPLES) public readonly currencyTuples: BehaviorSubject<CurrencyTuple[]>,
    @Inject(AMOUNT_ELEMENT) public readonly amountInput$: BehaviorSubject<HTMLInputElement | null>,
    @Inject(CURRENCY_ELEMENT) public readonly currencySelect$: BehaviorSubject<HTMLSelectElement | null>,
  ) {
  }

  public readonly writtenValue = new BehaviorSubject<Dinero | null>(null);

  private readonly dineroValue = merge(this.amountControl.valueChanges, this.currencyControl.valueChanges).pipe(switchMap(() => this.getAsDinero()));
  public readonly invalidCurrency: Observable<CurrencyTuple | null> = combineLatest([
    this.currencyControl.valueChanges.pipe(startWith(null), map(() => this.currencyControl.value as string)),
    this.currencyTuples,
  ]).pipe(
    map(([currency, currencies]) => currencies.some(c => c.code === currency) ? null : currency),
    map(code => code ? (getAsCurrencyTuple(code) || { code } as CurrencyTuple) : null),
  );
  public readonly showCurrencySelect = combineLatest([this.currencyTuples, this.invalidCurrency]).pipe(
    map(([currencyList, invalidCurrency]) => currencyList.length !== 1 || !!invalidCurrency),
  );
  public readonly amountControlValue = this.amountControl.valueChanges.pipe(
    startWith(null),
    map(() => this.amountControl.value as string),
  );
  private readonly currencyControlValue = this.currencyControl.valueChanges.pipe(
    startWith(null),
    map(() => this.currencyControl.value),
    map(code => code ? getAsCurrencyTuple(code) : null),
  );
  private readonly finalCurrencyValue = combineLatest([this.currencyControlValue, this.currencyTuples]).pipe(
    map(([currencyControlValue, currencyTuples]) => {
      if (!currencyControlValue) {
        if (currencyTuples.length === 1) {
          return currencyTuples[0];
        }
      }
      return currencyControlValue;
    }),
  );
  public readonly isEmpty = this.amountControlValue.pipe(map(amount => amount ? !amount.trim() : true));
  public readonly isNotEmpty = this.isEmpty.pipe(map(is => !is));
  public readonly cleaveOptions = this.finalCurrencyValue.pipe(
    map(codeTuple => ({
      numeral: true,
      numeralThousandsGroupStyle: 'thousand',
      prefix: codeTuple ? `${codeTuple.sign} ` : undefined,
      noImmediatePrefix: true,
    })),
  );
  private readonly changedValue = this.writtenValue.pipe(
    switchMap(writtenValue => concat(of(writtenValue), this.dineroValue).pipe(
      distinctUntilChangedDinero(),
      // We skip the written value
      skip(1),
    )),
  );
  public readonly changeRawValue = this.changedValue.pipe(
    map(dinero => dinero ? { amount: dinero.getAmount(), currency: dinero.getCurrency() } : null),
  );

  async getAsDinero(): Promise<Dinero.Dinero | null> {
    const amountStr: string = this.amountControl.value;

    // If no digits, its indiscernible
    if (amountStr && !amountStr.match(/\d+/)) {
      return null;
    }

    const currencyTuple = await promiseFirst(this.finalCurrencyValue);
    const currency = currencyTuple ? currencyTuple.code : null;

    if (amountStr && currency) {
      const amount = getAmount(amountStr);
      return DineroFactory({ amount, currency });
    }
    return null;
  }

  ngOnDestroy() {
  }
}
