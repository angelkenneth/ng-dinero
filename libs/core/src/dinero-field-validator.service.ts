import { Inject, Injectable, OnDestroy } from '@angular/core';
import { AbstractControl, FormControl, ValidationErrors, Validator } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { DineroFieldErrorCode } from './dinero-field.enums';
import { ValidationErrorItem } from './dinero-field.models';
import { AMOUNT_CONTROL, CURRENCY_CONTROL, CURRENCY_TUPLES } from './dinero-field.tokens';
import { CurrencyTuple } from './utils';


@Injectable({ providedIn: 'root' })
export class DineroFieldValidatorService implements Validator, OnDestroy {
  public propagateValidatorChange: () => void = () => void 0;

  constructor(
    @Inject(AMOUNT_CONTROL) public readonly amountControl: FormControl,
    @Inject(CURRENCY_CONTROL) public readonly currencyControl: FormControl,
    @Inject(CURRENCY_TUPLES) public readonly currencyTuples: BehaviorSubject<CurrencyTuple[]>,
  ) {
  }

  validate(control: AbstractControl): ValidationErrors | null {
    const errors: Partial<Record<DineroFieldErrorCode, ValidationErrorItem>> = {};
    const amountStr: string = this.amountControl.value;
    const currency: string = this.currencyControl.value;
    const currencyCodes = this.currencyTuples.value.map(t => t.code);

    if (amountStr) {
      if (!amountStr.match(/\d+/)) {
        errors[DineroFieldErrorCode.indiscernibleCurrencyAmount] = {
          path: [],
          type: DineroFieldErrorCode.indiscernibleCurrencyAmount,
          message: 'Indiscernible Currency Amount',
        };
      }
    }

    if (currency) {
      if (!currencyCodes.includes(currency)) {
        errors[DineroFieldErrorCode.invalidChoice] = {
          path: [],
          type: DineroFieldErrorCode.invalidChoice,
          message: `Currency ${currency} is not a valid choice`,
          context: { valid: currencyCodes },
        };
      }
    }

    if (Object.keys(errors).length > 0) {
      return errors;
    }
    return null;
  }

  registerOnValidatorChange(fn: () => void): void {
    this.propagateValidatorChange = fn;
  }

  ngOnDestroy() {
  }
}
