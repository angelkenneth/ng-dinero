export enum DineroFieldErrorCode {
  indiscernibleCurrencyAmount = 'indiscernibleCurrencyAmount',
  invalidChoice = 'invalidChoice',
  required = 'required',
}
